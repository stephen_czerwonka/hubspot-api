var path = require('path');
var request = require('sync-request');
var _request = require('request');
var hsToken = require('./getToken.js');
var fs = require('fs');


exports.upload = function (filePath){
  var url = "https://api.hubapi.com/filemanager/api/v2/files?access_token="+ hsToken.get();
  // url = 'http://requestb.in/xl20knxl';
  var req = _request.post(url, function(err,resp,body){
     if (err) {
        console.log('Error!');
      } else {
        console.log('URL: ' + JSON.parse(body).objects[0].url);
        // console.log('')
        
      }
      
  });
  
  var form = req.form();
  
  // form.append('my_field', 'my_value');

  form.append('files', fs.createReadStream(filePath));
  // form.append('folder_paths', '_iconfonts');
  // form.append('data', '{"folder_paths": "_iconfonts"' });
		// console.log(req.body.toString());
		// console.log(req.statusCode);
  return req;
}


exports.replace = function (id,filePath){
  var url = "https://api.hubapi.com/filemanager/api/v2/files/"+id+"?access_token="+ hsToken.get();
  // url = 'http://requestb.in/xl20knxl';
  var req = _request.post(url, function(err,resp,body){
     if (err) {
        console.log('Error!');
      } else {
        // console.log('URL: ' + body);
        // console.log('')
      }
      
  });
  var form = req.form();
  form.append('files', fs.createReadStream(filePath));
  return req;
}

exports.getMetaData = function(id){
    var url = 'https://api.hubapi.com/filemanager/api/v2/files/'+id+'?access_token='+ hsToken.get();
  // url = 'http://requestb.in/1n0z9ri1';
  var result = request('get', url);
  console.log(result.body.toString());
  return result;
  
}

