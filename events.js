var path = require('path');
var request = require('sync-request');
var hsToken = require('./getToken.js');


exports.trigger = function(str, email) {

  // console.log(str);
  // console.log(email);
  
  function createEventUrl(str, email) {
    var url = "http://track.hubspot.com/v1/event?_n=";
    url += encodeURIComponent(str);
    url += "&_a=325051&email=";
    url += encodeURIComponent(email);
    return url;
  }

  var url = createEventUrl(str, email)

  // async request

  // request('http://www.google.com', function(error, response, body) {
  //   if (!error && response.statusCode == 200) {
  //       return 'success';
  //   }
  // });
  console.log('Hubspot Event: ' + url);
  return request('get', url);
	

}


